package com.prous.sdfExporter.exec;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Array;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.prous.logger.Logger;
import com.prous.sdfExporter.beans.SdfObject;


public class Execution {
	private static Connection con;
	private static BufferedWriter out;
	public static SdfObject sdfInfo;
	private static Logger log;
	public static ResultSet rs;

	public static void main(String[] args) throws SQLException, IOException {
		Integer fetchSize = Integer.valueOf(args[3]);
		String fields = args[2].replaceAll(" ", "").toLowerCase();
		String query = args[1];
		String filepath = args[0];
		List<String> fieldsList = null;

		if (fields.isEmpty()) {
			fieldsList = new ArrayList<String>();
		} else {
			fieldsList = Arrays.asList(fields.split(","));
		}
		con = getDBConnection();
		log = new Logger(filepath.concat(".txt"));
		File f = new File(filepath);

		FileWriter writer = null;
		try {
			writer = new FileWriter(f, true);
		} catch (IOException e) {
			e.printStackTrace();
		}

		out = new BufferedWriter(writer);

		try {
			generateSdfReport(query, fetchSize, fieldsList);

		} catch (Exception e) {
			con.close();
			out.close();
			e.printStackTrace();
		}
		con.close();
		out.close();

	}

	public static void generateSdfReport(String finalQuery, Integer fetchSize,
			List<String> fields) throws Exception {

		System.out.println(finalQuery);
		log.write(finalQuery);

		Date startingDate = new Date();

		Statement stmt = con.createStatement();
		stmt.setFetchSize(fetchSize);
		rs = stmt.executeQuery(finalQuery);
		int status = 0;
		int regAmm = 0;
		System.out.println("Starting export at " + startingDate.toString());
		log.write("Starting export at " + startingDate.toString());

		while (rs.next()) {
			sdfInfo = new SdfObject();
			status++;
			regAmm++;

			if (fields.isEmpty() || fields.contains("structure")) {

				getStructure(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("entry_number")) {
				sdfInfo.setEntryNumber(new BigInteger(rs
						.getString("ENTRY_NUMBER")));
			}

			if (fields.isEmpty() || fields.contains("chemical_name")) {

				getChemicalName(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("organizations")) {

				getOrganizations(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("isinchiclob")) {
				sdfInfo.setIsInchiClob(!(rs.getString("PIC_IS_INCHI_CLOB") == "N"));
			}

			if (fields.isEmpty() || fields.contains("picinchi")) {
				sdfInfo.setPicInchi(rs.getString("PIC_INCHI"));
			}

			if (fields.isEmpty() || fields.contains("picinchikey")) {
				sdfInfo.setInchiKey(rs.getString("PIC_INCHI_KEY"));
			}

			if (fields.isEmpty() || fields.contains("under_development")) {
				sdfInfo.setIsUnderActiveDevelopment(!(rs
						.getString("UNDER_ACTIVE_DEVELOPMENT").equals("N")));
			}

			if (fields.isEmpty() || fields.contains("highest_phase")) {
				sdfInfo.setHighestPhase(rs.getString("HIGHEST_PHASE"));
			}

			if (fields.isEmpty() || fields.contains("code_names")) {

				getCodeNames(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("generic_names")) {

				getGenericNames(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("brand_names")) {

				getBrandNames(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("therapeutic_groups")) {

				getTherapeuticGroups(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("cellular_mechanisms")) {

				getCellularMechanisms(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("molecular_mechanisms")) {

				getMolecularMechanisms(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("product_cat")) {

				getProductCategories(sdfInfo, rs);

			}

			if (fields.isEmpty() || fields.contains("molecular_formula")) {
				sdfInfo.setMolecularFormula(rs.getString("molecular_formula"));
			}

			if (fields.isEmpty() || fields.contains("mol_weight")) {
				String molWeight = rs.getString("mol_weight");
				if (!molWeight.isEmpty()) {
					sdfInfo.setMolWeight(Double.valueOf(molWeight));
				} else {
					sdfInfo.setMolWeight(0d);

				}
			}

			if (fields.isEmpty() || fields.contains("condition")) {
				sdfInfo.setCondition(listString(rs.getArray("condition")));
			}

			if (fields.isEmpty() || fields.contains("related_basic_patent")) {
				sdfInfo.setRelatedBasicPatent(listString(rs
						.getArray("related_basic_patent")));
			}

			if (fields.isEmpty() || fields.contains("lead_compound")) {
				sdfInfo.setLeadCompound(!(rs.getString("lead_compound")
						.equals("N")));
			}

			if (fields.isEmpty() || fields.contains("available_since")) {
				sdfInfo.setAvailableSince(rs.getString("available_since"));
			}

			if (fields.isEmpty() || fields.contains("patent_numbers")) {
				sdfInfo.setRelatedBasicPatent(listString(rs
						.getArray("patent_numbers")));
			}
			
			if (fields.isEmpty() || fields.contains("patent_numbers")) {
				sdfInfo.setRelatedBasicPatent(listString(rs
						.getArray("patent_numbers")));
			}
			
			if (fields.isEmpty() || fields.contains("product_summary")) {
				sdfInfo.setSummary(rs
						.getString("product_summary"));
			}
			
			

			System.out.println("Adding emr_id=" + sdfInfo.getEntryNumber()
					+ " register number:" + regAmm);
			log.write("Adding entry number=" + sdfInfo.getEntryNumber()
					+ " register number:" + regAmm);
			addRegister(sdfInfo);
			out.flush();
			if (status == 10000) {

				// in milliseconds
				long diff = new Date().getTime() - startingDate.getTime();

				long diffMinutes = diff / (60 * 1000);
				System.out.println("It took " + diffMinutes
						+ " minutes to this point");
				log.write("It took " + diffMinutes + " minutes to this point");
				System.gc();
				status = 0;
			}
		}
		long diff = new Date().getTime() - startingDate.getTime();

		long diffMinutes = diff / (60 * 1000);
		log.write(regAmm + " registers in " + diffMinutes);
	}

	private static void addRegister(SdfObject sdf) throws IOException {
		if (sdf.getEntryNumber() != null) {
			out.write(sdf.getEntryNumber().toString());
		}
		if (sdf.getMolfile() != null && !sdf.getMolfile().isEmpty()) {
			out.write(sdf.getMolfile());

		}

		if (sdf.getEntryNumber() != null) {
			addField("Entry_Number", sdf.getEntryNumber(), sdf.getEntryNumber());
			addField(
					"Integrity_Link",
					"https://integrity.thomson-pharma.com/directAccess/integrityDirect.jsp?p_type=PROCARD&p_id="
							+ sdf.getEntryNumber(), sdf.getEntryNumber());
		}

		if (!sdf.getMechanismsOfAction().isEmpty()) {
			addListField("Mechanism_of_Action", sdf.getMechanismsOfAction(),
					sdf.getEntryNumber());
		}
		
		if (!sdf.getChemicalName().isEmpty()) {
			addListField("Chemical_Name", sdf.getChemicalName(),
					sdf.getEntryNumber());
		}

		if (sdf.getPicInchi() != null && !sdf.getPicInchi().isEmpty()) {
			addField("InChI", sdf.getPicInchi(), sdf.getEntryNumber());
		}

		if (sdf.getInchiKey() != null && !sdf.getInchiKey().isEmpty()) {
			addField("InChIKey", sdf.getInchiKey(), sdf.getEntryNumber());
		}

		if (sdf.getIsUnderActiveDevelopment() != null) {
			addField("Under_Active_Development",
					sdf.getIsUnderActiveDevelopment() ? "YES" : "NO",
					sdf.getEntryNumber());
		}

		if (sdf.getHighestPhase() != null && !sdf.getHighestPhase().isEmpty()) {
			addField("Highest_Phase", sdf.getHighestPhase(),
					sdf.getEntryNumber());
		}
		if (!sdf.getGenericNames().isEmpty()) {
			addListField("Generic_Name", sdf.getGenericNames(),
					sdf.getEntryNumber());
		}
		if (!sdf.getBrandNames().isEmpty()) {
			addListField("Brand_Name", sdf.getBrandNames(),
					sdf.getEntryNumber());
		}

		if (!sdf.getOrganizations().isEmpty()) {
			addListField("Organization", sdf.getOrganizations(),
					sdf.getEntryNumber());
		}

		if (!sdf.getTherapeuticGroups().isEmpty()) {
			addListField("Therapeutic_Group", sdf.getTherapeuticGroups(),
					sdf.getEntryNumber());
		}
//
//		if (!sdf.getMolecularMechanisms().isEmpty()) {
//			addListField("Mollecular_Mech_Action",
//					sdf.getMolecularMechanisms(), sdf.getEntryNumber());
//		}
//
//		if (!sdf.getCellularMechanisms().isEmpty()) {
//			addListField("Cellular_Mech_Action", sdf.getCellularMechanisms(),
//					sdf.getEntryNumber());
//		}
		
		
		

		if (!sdf.getProductCategories().isEmpty()) {
			addListField("Category", sdf.getProductCategories(),
					sdf.getEntryNumber());
		}

		if (!sdf.getCodeNames().isEmpty()) {
			addListField("Company_Code", sdf.getCodeNames(),
					sdf.getEntryNumber());
		}

		
		
		
		if (sdf.getMolecularFormula() != null
				&& !sdf.getMolecularFormula().isEmpty()) {
			addField("MOLECULAR_FORMULA", sdf.getMolecularFormula(),
					sdf.getEntryNumber());
		}

		if (sdf.getMolWeight()!=null) {
			addField("MOLECULAR_WEIGHT", sdf.getMolWeight(),
					sdf.getEntryNumber());
		}

		if (!sdf.getCondition().isEmpty()) {
			addListField("CONDITION", sdf.getCondition(),
					sdf.getEntryNumber());
		}

		if (!sdf.getRelatedBasicPatent().isEmpty()) {
			addListField("RELATED_BASIC_PATENT", sdf.getRelatedBasicPatent(),
					sdf.getEntryNumber());
		}

		if (sdf.getLeadCompound()!=null) {
			addField("Lead_Compound",
					sdf.getLeadCompound() ? "YES" : "NO",
					sdf.getEntryNumber());
		}

		if (sdf.getAvailableSince()!=null && !sdf.getAvailableSince().isEmpty()) {
			addField("AVAILABLE_SINCE", sdf.getAvailableSince(),
					sdf.getEntryNumber());
		}

		if (!sdf.getPatentNumbers().isEmpty()) {
			addListField("PATENT_NUMBERS", sdf.getPatentNumbers(),
					sdf.getEntryNumber());
		}
		
		if (sdf.getSummary() != null && !sdf.getSummary().isEmpty()) {
			addField("SUMMARY", sdf.getSummary(), sdf.getEntryNumber());
		}

		out.write("$$$$");
		out.newLine();

	}

	private static Connection getDBConnection() throws SQLException,
			IOException {
		Properties prop = new Properties();

		prop.load(Execution.class.getResourceAsStream("config.properties"));

		DriverManager.registerDriver(new oracle.jdbc.OracleDriver());

		Connection conn = null;
		String jdbcUrl = "jdbc:oracle:thin:@" + prop.getProperty("host") + ":"
				+ prop.getProperty("port") + ":" + prop.getProperty("sid");

		String user = prop.getProperty("user");
		String password = prop.getProperty("password");

		conn = DriverManager.getConnection(jdbcUrl, user, password);

		conn.setAutoCommit(false);

		return conn;
	}

	private static void newLine() throws IOException {
		out.newLine();
		out.newLine();
	}

	private static void addField(String name, Object value, BigInteger entry)
			throws IOException {
		out.write(">  <" + name + "> (" + entry + ")");
		out.newLine();
		out.write(value.toString());
		newLine();
	}

	@SuppressWarnings("rawtypes")
	private static void addListField(String name, List values, BigInteger entry)
			throws IOException {
		out.write(">  <" + name + "> (" + entry + ")");

		for (Object object : values) {
			out.newLine();
			out.write(object.toString());

		}
		newLine();
	}

	private static List<String> listString(Array array) {

		String[] a;
		try {
			a = (String[]) array.getArray();
		} catch (Exception e) {

			return new ArrayList<String>();
		}
		return Arrays.asList(a);
	}
	
	private static List<String> listClob(Array array) throws Exception{
		ResultSet rs = array.getResultSet();
		List<String> chemicalName = new ArrayList<String>();
		while (rs.next()) {
			Clob myClob = rs.getClob(2);
			chemicalName.add(myClob.getSubString(1, (int) myClob.length()));
		}
		
		return chemicalName;
	}

	public static void getStructure(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {
		sdfInfo.setMolfile(rs.getString("molfile"));

	}

	public static void getChemicalName(SdfObject sdfInfo, ResultSet rs)
			throws Exception {
		sdfInfo.setChemicalName(listClob(rs.getArray("CHEMICAL_NAME")));
	}

	public static void getOrganizations(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {
		sdfInfo.setOrganizations(listString(rs.getArray("ORGANIZATIONS")));
	}

	public static void getCodeNames(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {
		sdfInfo.setCodeNames(listString(rs.getArray("CODE_NAMES")));
	}

	public static void getGenericNames(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {
		sdfInfo.setGenericNames(listString(rs.getArray("GENERIC_NAMES")));
	}

	public static void getBrandNames(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {
		sdfInfo.setBrandNames(listString(rs.getArray("BRAND_NAMES")));
	}

	public static void getTherapeuticGroups(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {
		sdfInfo.setTherapeuticGroups(listString(rs
				.getArray("THERAPEUTIC_GROUPS")));
	}

	public static void getCellularMechanisms(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {

		sdfInfo.setCellularMechanisms(listString(rs
				.getArray("cellular_mechanisms")));
	}

	public static void getMolecularMechanisms(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {

		sdfInfo.setMolecularMechanisms(listString(rs
				.getArray("molecular_mechanisms")));
	}

	public static void getProductCategories(SdfObject sdfInfo, ResultSet rs)
			throws SQLException {

		sdfInfo.setProductCategories(listString(rs
				.getArray("PRODUCT_CATEGORIES")));
	}
}
