package com.prous.sdfExporter.beans;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class SdfObject {
	private String molfile;
	private BigInteger entryNumber;
	private List<String> chemicalName;
	private Boolean isInchiCLOB;
	private String picInchi;
	private String inchiKey;
	private Boolean isUnderActiveDevelopment;
	private String highestPhase;
	private List<String> codeNames;
	private List<String> genericNames;
	private List<String> brandNames;
	private List<String> therapeuticGroups;
	private List<String> molecularMechanisms;
	private List<String> cellularMechanisms;
	private String productSummary;
	private List<String> productCategories;
	private List<String> organizations;
	private String summary;
	private String molecularFormula;
	private Double molWeight;
	private List<String> condition;
	private List<String> relatedBasicPatent;
	private Boolean leadCompound;
	private String availableSince;
	private List<String> patentNumbers;

	public List<String> getRelatedBasicPatent() {
		if (this.relatedBasicPatent == null) {
			this.relatedBasicPatent = new ArrayList<String>();
		}
		return relatedBasicPatent;
	}

	public void setRelatedBasicPatent(List<String> relatedBasicPatent) {
		this.relatedBasicPatent = relatedBasicPatent;
	}

	public Boolean getLeadCompound() {
		return leadCompound;
	}

	public void setLeadCompound(Boolean leadCompound) {
		this.leadCompound = leadCompound;
	}

	public String getAvailableSince() {
		return availableSince;
	}

	public void setAvailableSince(String availableSince) {
		this.availableSince = availableSince;
	}

	public List<String> getPatentNumbers() {
		if (this.patentNumbers == null) {
			this.patentNumbers = new ArrayList<String>();
		}
		return patentNumbers;
	}

	public void setPatentNumbers(List<String> patentNumbers) {
		this.patentNumbers = patentNumbers;
	}

	public String getMolfile() {
		return molfile;
	}

	public void setMolfile(String molfile) {
		this.molfile = molfile;
	}

	public BigInteger getEntryNumber() {
		return entryNumber;
	}

	public void setEntryNumber(BigInteger entryNumber) {
		this.entryNumber = entryNumber;
	}

	public List<String> getChemicalName() {
		if (this.chemicalName == null) {
			this.chemicalName = new ArrayList<String>();
		}
		return chemicalName;
	}

	public void setChemicalName(List<String> chemicalName) {
		this.chemicalName = chemicalName;
	}

	public String getPicInchi() {
		return picInchi;
	}

	public void setPicInchi(String picInchi) {
		this.picInchi = picInchi;
	}

	public String getInchiKey() {
		return inchiKey;
	}

	public void setInchiKey(String inchiKey) {
		this.inchiKey = inchiKey;
	}

	public Boolean getIsUnderActiveDevelopment() {
		return isUnderActiveDevelopment;
	}

	public void setIsUnderActiveDevelopment(Boolean isUnderActiveDevelopment) {
		this.isUnderActiveDevelopment = isUnderActiveDevelopment;
	}

	public String getHighestPhase() {
		return highestPhase;
	}

	public void setHighestPhase(String highestPhase) {
		this.highestPhase = highestPhase;
	}

	public List<String> getCodeNames() {
		if (this.codeNames == null) {
			this.codeNames = new ArrayList<String>();
		}
		return codeNames;
	}

	public void setCodeNames(List<String> codeNames) {

		this.codeNames = codeNames;
	}

	public List<String> getGenericNames() {
		if (this.genericNames == null) {
			this.genericNames = new ArrayList<String>();
		}
		return genericNames;
	}

	public void setGenericNames(List<String> genericNames) {

		this.genericNames = genericNames;
	}

	public List<String> getBrandNames() {
		if (this.brandNames == null) {
			this.brandNames = new ArrayList<String>();
		}
		return brandNames;
	}

	public void setBrandNames(List<String> brandNames) {

		this.brandNames = brandNames;
	}

	public List<String> getTherapeuticGroups() {
		if (this.therapeuticGroups == null) {
			this.therapeuticGroups = new ArrayList<String>();
		}
		return therapeuticGroups;
	}

	public void setTherapeuticGroups(List<String> therapeuticGroups) {

		this.therapeuticGroups = therapeuticGroups;
	}

	public List<String> getMolecularMechanisms() {
		if (this.molecularMechanisms == null) {
			this.molecularMechanisms = new ArrayList<String>();
		}
		return molecularMechanisms;
	}

	public void setMolecularMechanisms(List<String> molecularMechanisms) {

		this.molecularMechanisms = molecularMechanisms;
	}

	public List<String> getCellularMechanisms() {
		if (this.cellularMechanisms == null) {
			this.cellularMechanisms = new ArrayList<String>();
		}
		return cellularMechanisms;
	}

	public void setCellularMechanisms(List<String> cellularMechanisms) {

		this.cellularMechanisms = cellularMechanisms;
	}

	public String getProductSummary() {
		return productSummary;
	}

	public void setProductSummary(String productSummary) {
		this.productSummary = productSummary;
	}

	public List<String> getProductCategories() {
		if (this.productCategories == null) {
			this.productCategories = new ArrayList<String>();
		}
		return productCategories;
	}

	public void setProductCategories(List<String> productCategories) {

		this.productCategories = productCategories;
	}

	public void setIsInchiClob(Boolean b) {

	}

	public Boolean getIsInchiCLOB() {
		return isInchiCLOB;
	}

	public void setIsInchiCLOB(Boolean isInchiCLOB) {
		this.isInchiCLOB = isInchiCLOB;
	}

	public List<String> getOrganizations() {
		if (this.organizations == null) {
			this.organizations = new ArrayList<String>();
		}
		return organizations;
	}

	public void setOrganizations(List<String> organization) {
		this.organizations = organization;
	}

	public String getMolecularFormula() {
		return molecularFormula;
	}

	public void setMolecularFormula(String molecularFormula) {
		this.molecularFormula = molecularFormula;
	}

	public Double getMolWeight() {
		return molWeight;
	}

	public void setMolWeight(Double molWeight) {
		this.molWeight = molWeight;
	}

	public List<String> getCondition() {
		if (this.condition == null) {
			this.condition = new ArrayList<String>();
		}
		return condition;
	}

	public void setCondition(List<String> condition) {
		this.condition = condition;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	public List<String> getMechanismsOfAction(){
		List<String> a = new ArrayList<String>();
		a.addAll(getCellularMechanisms());
		a.addAll(getMolecularMechanisms());
		return a;

	
	}
}


