package com.prous.sdfExporter.helpers;

import java.sql.Blob;
import java.sql.SQLException;

public class MolStringification {
	public static String getBlobAsString(Blob blob) {
		if (blob == null) {
			return null;
		}
		byte[] bdata = null;
		try {
			bdata = blob.getBytes(1, (int) blob.length());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		String data = new String(bdata);

		// data = data.replace(System.getProperty("line.separator"), "\\r\\n");
		data = data.replaceAll("\\r\\n|\\r|\\n", "\\\\r\\\\n");

		return data;
	}

	public static String getBlobAsStringNoScaped(Blob blob) {
		if (blob == null) {
			return null;
		}
		byte[] bdata = null;
		try {
			bdata = blob.getBytes(1, (int) blob.length());
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		String data = new String(bdata);
		data = data.replaceAll("\\r\\n|\\r|\\n", "\r\n");
		return data;
	}
}
